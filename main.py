# -*- coding: utf-8 -*-
from PIL import Image
import urllib.request
from datetime import datetime
from bs4 import BeautifulSoup
from flask import Flask
from slack import WebClient
from slackeventsapi import SlackEventAdapter
from urllib import parse
from urllib.request import FancyURLopener
from config import *

app = Flask(__name__)
# /listening 으로 슬랙 이벤트를 받습니다.
slack_events_adaptor = SlackEventAdapter(SLACK_SIGNING_SECRET, "/listening", app)
slack_web_client = WebClient(token=SLACK_TOKEN)

class AppURLopener(FancyURLopener):
   	version = "Mozilla/5.0"


# 음악크롤링
def _crawl_music(text):


    url = "https://www.genie.co.kr/playlist/tags"

    default_agent = FancyURLopener().version
    changed_agent = AppURLopener().version
    print(default_agent, "->", changed_agent)


    html = AppURLopener().open(url)
    soup = BeautifulSoup(html, "html.parser")


    url2 = ""
    musicList = []
    #사용자가 원하는 태그의 주소찾기, 주소를 url2에 넣어줌.
    for i, data in enumerate(soup.find_all('a',class_="tag-key")):
        if(str(data.get_text()) == text[int(text.find("> ")+5):]) :
            url2 = "https://www.genie.co.kr/playlist/tags?tags=" + data.attrs['onclick'][14:20] + "&sortOrd=PPA"
            break

    if not url2:
        text=" "
        return "\'음악 (휴식/드라이브/산책/집/운동/시상식/거리/하우스파티/클럽/고백/해변/공연/라운지/봄/여름/가을/겨울/달리기/걷기/노래방/방송/밝은/신나는/따뜻한/편안한/그루브한/부드러운/로맨틱한/매혹적인/영화음악/잔잔한/댄서블한/달콤한/몽환적인/시원한/애절한/어두운/연주음악/발렌타인데이/화이트데이)\'의 양식을 지켜주세요."

    musicList.append("' *#" + text[int(text.find("> ") + 5):] + "* ' 에 알맞은 노래를 찾았습니다!")

    html = AppURLopener().open(url2)
    soup = BeautifulSoup(html, "html.parser")

    #태그의 재생목록 하나.
    for _, data in enumerate(soup.find_all('div',class_="title")):
        for i, data2 in enumerate(data.find_all('a')):
            url3 = "https://www.genie.co.kr/playlist/detailView?plmSeq="+data2.attrs['onclick'][25:29]
        break

    html = AppURLopener().open(url3)
    soup = BeautifulSoup(html, "html.parser")


    #재생목록 하나의 곡목록 리스트업
    for i, data in enumerate(soup.find_all('a',class_="title ellipsis")):
        if(data.get_text(strip= True).startswith('TITLE')):
            tmp = str(data.get_text(strip = True))[5:]
        else:
            tmp = data.get_text(strip = True)
        musicList.append(tmp)


    return "\n".join(musicList)

#우산크롤링
def _crawl_umbrella(text):

    if "우산 서울" in text :
        url = "http://www.kma.go.kr/wid/queryDFSRSS.jsp?zone=1114055000"
        region = "서울"
    elif "우산 경기도" in text:
        url = "http://www.kma.go.kr/wid/queryDFSRSS.jsp?zone=4113566500"
        region = "경기도"
    elif "우산 강원도" in text:
        url = "http://www.kma.go.kr/wid/queryDFSRSS.jsp?zone=4211062000"
        region = "강원도"
    elif "우산 충청북도" in text:
        url = "http://www.kma.go.kr/wid/queryDFSRSS.jsp?zone=4313054000"
        region = "충청북도"
    elif "우산 충청남도" in text:
        url = "http://www.kma.go.kr/wid/queryDFSRSS.jsp?zone=4427031000"
        region = "충청남도"
    elif "우산 전라북도" in text:
        url = "http://www.kma.go.kr/wid/queryDFSRSS.jsp?zone=4519055000"
        region = "전라북도"
    elif "우산 전라남도" in text:
        url = "http://www.kma.go.kr/wid/queryDFSRSS.jsp?zone=4617025000"
        region = "전라남도"
    elif "우산 경상북도" in text:
        url = "http://www.kma.go.kr/wid/queryDFSRSS.jsp?zone=4719055100"
        region = "경상북도"
    elif "우산 경상남도" in text:
        url = "http://www.kma.go.kr/wid/queryDFSRSS.jsp?zone=4831034000"
        region = "경상남도"
    elif "우산 제주도" in text:
        url = "http://www.kma.go.kr/wid/queryDFSRSS.jsp?zone=5013059000"
        region = "제주도"
    else :
        text = ""
        return "\"우산 (서울/경기도/강원도/충청북도/충청남도/전라북도/전라남도/경상북도/경상남도/제주도)\"의 형식을 따라주세요!"

    sourcecode = urllib.request.urlopen(url)
    soup = BeautifulSoup(sourcecode, "html.parser")

    keywords = []
    pop_prob = ""
    pop_avag = 0

    keywords.append("지금으로부터 약 12시간동안의 *"+region+"* 의 날씨를 안내 해드리겠습니다! ")
    for i,data in  enumerate(soup.find_all('data')):
        if(i>=5) :
            break;
        pop_avag += int(data.pop.string)
        if(int(data.pop.string)>=50) :
            pop_prob += (str(int(datetime.today().day) + int(data.day.string)) + "일 " +
                        data.hour.string + "시")



    pop_avag /= 5

    if pop_prob :
        keywords.append("강수확률이 50퍼센트이상인 시간은 ["+pop_prob+"] 입니다. \n따라서 *우산을 챙겨가세요^_^* ")
    else :
        keywords.append("12시간동안의 평균강수확률은 *"+str(pop_avag)+"퍼센트* 입니다. 따라서 *우산을 챙길 필요가 없습니다!* ")

    # 키워드 리스트를 문자열로 만듭니다.
    return '\n'.join(keywords)

def _crawl_cody(text):
    if "코디 서울" in text :
        url = "http://www.kma.go.kr/wid/queryDFSRSS.jsp?zone=1114055000"
        region = "서울"
    elif "코디 경기도" in text:
        url = "http://www.kma.go.kr/wid/queryDFSRSS.jsp?zone=4113566500"
        region = "경기도"
    elif "코디 강원도" in text:
        url = "http://www.kma.go.kr/wid/queryDFSRSS.jsp?zone=4211062000"
        region = "강원도"
    elif "코디 충청북도" in text:
        url = "http://www.kma.go.kr/wid/queryDFSRSS.jsp?zone=4313054000"
        region = "충청북도"
    elif "코디 충청남도" in text:
        url = "http://www.kma.go.kr/wid/queryDFSRSS.jsp?zone=4427031000"
        region = "충청남도"
    elif "코디 전라북도" in text:
        url = "http://www.kma.go.kr/wid/queryDFSRSS.jsp?zone=4519055000"
        region = "전라북도"
    elif "코디 전라남도" in text:
        url = "http://www.kma.go.kr/wid/queryDFSRSS.jsp?zone=4617025000"
        region = "전라남도"
    elif "코디 경상북도" in text:
        url = "http://www.kma.go.kr/wid/queryDFSRSS.jsp?zone=4719055100"
        region = "경상북도"
    elif "코디 경상남도" in text:
        url = "http://www.kma.go.kr/wid/queryDFSRSS.jsp?zone=4831034000"
        region = "경상남도"
    elif "코디 제주도" in text:
        url = "http://www.kma.go.kr/wid/queryDFSRSS.jsp?zone=5013059000"
        region = "제주도"
    else :
        text = ""
        return "\"코디 (서울/경기도/강원도/충청북도/충청남도/전라북도/전라남도/경상북도/경상남도/제주도)\"의 형식을 따라주세요!"

    sourcecode = urllib.request.urlopen(url)
    soup = BeautifulSoup(sourcecode, "html.parser")

    keywords = []
    temp = []
    avg_temp = 0

    keywords.append("지금 시각이후의 오늘날씨를 알려드리겠습니다.")
    for i,data in  enumerate(soup.find_all('data')):

        temp.append(float(data.temp.string))
        if (int(data.hour.string) == 24):
             break;

    for tmp in temp:
        avg_temp += tmp

    avg_temp /= len(temp)

    if avg_temp >= 28:
        cloth = "민소매, 반팔, 반바지, 원피스"
    elif avg_temp >= 23:
        cloth = "반팔, 얇은셔츠, 반바지, 면바지"
    elif avg_temp >= 20:
        cloth = "얇은 가디건, 긴팔, 면바지, 청바지"
    elif avg_temp >= 17:
        cloth = "얇은 니트, 맨투맨, 가디건, 청바지"
    elif avg_temp >= 12:
        cloth = "자켓, 가디건, 야상, 스타킹, 청바지, 면바지"
    elif avg_temp >= 9:
        cloth = "자켓, 트렌치코트, 야상, 니트, 청바지, 스타킹"
    elif avg_temp >= 5:
        cloth = "코트, 가죽자켓, 히트텍, 니트, 레깅스"
    else :
        cloth = "패딩, 두꺼운코트, 목도리, 기모제품"


    keywords.append("오늘의 최저기온은 '"+str(min(temp))+"도'이고, 최고기온은 '"+str(max(temp))+"도'입니다.")
    keywords.append("오늘의 평균기온은 '"+str(format(avg_temp,'.1f'))+"도'이기 때문에 *["+cloth+"]* 을 추천합니다.")

    # 키워드 리스트를 문자열로 만듭니다.
    return '\n'.join(keywords)


def _delicious_food_chart(text):
   storeName = text[16:]
   url = "https://www.mangoplate.com/search/" + parse.quote(storeName)

   default_agent = FancyURLopener().version
   changed_agent = AppURLopener().version
   print(default_agent, "->", changed_agent)

   html = AppURLopener().open(url)
   soup = BeautifulSoup(html, "html.parser")

   keywords = []
   keywords.append("*" + storeName + " 맛집 Best 5*\n")

   for i, keyword in enumerate(soup.find_all("h2", class_="title")):
       if i < 5 :
           strr = str(i + 1) + "위 : *" + keyword.get_text(strip=True) + "*"
           keywords.append(strr.replace("\n", ""))

   for i, keyword in enumerate(soup.find_all("p", class_="etc")):
       if i < 5 :
           strr = "-" + keyword.find("span").get_text()
           keywords[(i + 1)] += strr

   for i, keyword in enumerate(soup.find_all("strong", class_="search_point")):
       if i < 5:
           strr = "/평점:" + keyword.get_text(strip=True)
           keywords[(i + 1)] += strr

   # 키워드 리스트를 문자열로 만듭니다.
   return '\n'.join(keywords)

def _menu_food_chart(text):
  # 여기에 함수를 구현해봅시다.
   storeName = text[16:]

   url = "https://m.search.naver.com/search.naver?query=" + parse.quote(storeName)
   # print(url)

   sourcecode = urllib.request.urlopen(url).read()
   soup = BeautifulSoup(sourcecode, "html.parser")

   orderUrl = soup.find("div", class_="nsite_url").get_text(strip=True)
   keywords = []
   keywords.append("*" + storeName + " 메뉴 Best 5*\n_주문하기_ : " + orderUrl + "\n")

   for i, keyword in enumerate(soup.find_all("span", class_="menu_name")):
       if i < 5 :
           strr = str(i + 1) + "위 : *" + keyword.get_text(strip=True) + "*"
           keywords.append(strr.replace("\n", ""))

   for i, keyword in enumerate(soup.find_all("div", class_="price")):
       if i < 5 :
           strr = " /가격: " + keyword.get_text(strip=True)
           keywords[(i + 1)] += strr

   # keywords.append("\n 주문을 원하시면 브랜드명>메뉴번호를 입력해 주세요")

   # 키워드 리스트를 문자열로 만듭니다.
   return '\n'.join(keywords)

# 챗봇이 멘션을 받았을 경우
@slack_events_adaptor.on("app_mention")
def app_mentioned(event_data):
    channel = event_data["event"]["channel"]
    text = event_data["event"]["text"]

    if not (text[int(text.find(">")+1):]):
        message = "choiceBot에서 사용가능한 키워드\n=============================\n1.메뉴\n2.맛집\n3.코디\n4.음악\n5.우산\n=============================\n"

    elif (text[int(text.find("> ")+2):] == '음악'):
        message = "\'음악 (휴식/드라이브/산책/집/운동/시상식/거리/하우스파티/클럽/고백/해변/공연/라운지/봄/여름/가을/겨울/달리기/걷기/노래방/방송/밝은/신나는/따뜻한/편안한/그루브한/부드러운/로맨틱한/매혹적인/영화음악/잔잔한/댄서블한/달콤한/몽환적인/시원한/애절한/어두운/연주음악/발렌타인데이/화이트데이)\'의 양식을 지켜주세요."
    elif (text[int(text.find("> ")+2):].startswith('음악')):
        message = _crawl_music(text)
    elif (text[int(text.find("> ")+2):] == ('우산')):
        message = "\"우산 (서울/경기도/강원도/충청북도/충청남도/전라북도/전라남도/경상북도/경상남도/제주도)\"의 양식을 따라주세요!"
    elif (text[int(text.find("> ")+2):].startswith('우산')):
        message = _crawl_umbrella(text)
    elif (text[int(text.find("> ") + 2):] == ('코디')):
        message = "\"코디 (서울/경기도/강원도/충청북도/충청남도/전라북도/전라남도/경상북도/경상남도/제주도)\"의 양식을 따라주세요!"
    elif (text[int(text.find("> ") + 2):].startswith('코디')):
        message = _crawl_cody(text)
    elif (text[int(text.find("> ") + 2):] == ('맛집')):
        message = "\"맛집 (지역 or 음식이름)\"의 양식을 따라주세요!"
    elif (text[int(text.find("> ") + 2):].startswith('맛집')):
        message = _delicious_food_chart(text)
    elif (text[int(text.find("> ") + 2):] == ('메뉴')):
        message = "\"메뉴 (음식점이름)\"의 양식을 따라주세요!"
    elif (text[int(text.find("> ") + 2):].startswith('메뉴')):
        message = _menu_food_chart(text)
    else:
        message = "choiceBot에서 사용가능한 키워드\n=============================\n1.메뉴\n2.맛집\n3.코디\n4.음악\n5.우산\n=============================\n"


    myImage = Image.open('image.png')

    slack_web_client.chat_postMessage(
        channel=channel,
        text=message,
        image_url ="./image.png"

    )


# / 로 접속하면 서버가 준비되었다고 알려줍니다.
@app.route("/", methods=["GET"])
def index():
    return "<h1>Server is ready.</h1>"


if __name__ == '__main__':
    app.run('0.0.0.0', port=5000)
